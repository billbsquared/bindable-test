import { Component } from '@angular/core';

@Component({
  selector: 'app-home-page',
  template: `
    <p>
      Thanks for taking a look.
    </p>

    <p>This is a functional UI, apologies for any bugs you may run into.</p>
  `,
  styles: [],
})
export class HomePageComponent {

  constructor() { }

}
