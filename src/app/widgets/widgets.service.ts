import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export interface Widgets {
  id: number;
  partNumber: string;
  name: string;
  manufacturer: string;
}

export interface WidgetDetails extends Widgets {
  description?: string;
}

const WIDGETS: Widgets[] = [
  {
    id: 10001,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
  {
    id: 10002,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
  {
    id: 10003,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
  {
    id: 10004,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
  {
    id: 10005,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
  {
    id: 10006,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
  {
    id: 10007,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
  {
    id: 10008,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
  {
    id: 10009,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
  {
    id: 10010,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
  {
    id: 10011,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },

  {
    id: 10012,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
  {
    id: 10013,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
  {
    id: 10014,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
  {
    id: 10015,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
  {
    id: 10016,
    partNumber: 'abcd1234',
    name: 'A part',
    manufacturer: 'Acme',
  },
];

@Injectable({
  providedIn: 'root',
})
export class WidgetsService {

  private widgetsUrl = 'http://localhost:8888/widgets';
  private widgetUrl = 'http://localhost:8888/widget';
  private readonly corsHeaders: HttpHeaders;

  constructor(
    private http: HttpClient,
  ) {
    this.corsHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE',
    });
  }

  getWidgets(): Observable<Widgets[]> {
    return this.http.get<Widgets[]>(this.widgetsUrl);
  }

  getDetails(id: number): Observable<WidgetDetails> {
    const fullUrl = [this.widgetUrl, id].join('/');
    return this.http.get<WidgetDetails>(fullUrl, {headers: this.corsHeaders});
  }
}
