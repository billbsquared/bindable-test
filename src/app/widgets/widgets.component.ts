import { Component, OnInit } from '@angular/core';
import { Widgets, WidgetsService } from './widgets.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-widgets',
  template: `
    <div class="row" *ngIf="!widgets">
      We seem to have a problem loading the widgets.
    </div>
    <div class="row" *ngIf="widgets">
      <div class="app-widget-list">
        <mat-nav-list>
          <a mat-list-item *ngFor="let widget of widgets" (click)="setWidget(widget.id)" routerLink="/widgets/{{ widget.id }}">
            {{ widget.id }}
          </a>
        </mat-nav-list>
      </div>
      <app-widget-details [widgetId]="widgetId"></app-widget-details>
      <p *ngIf="!widgetId">
        Click on a widget to view the details.
      </p>
    </div>
  `,
  styles: [],
})
export class WidgetsComponent implements OnInit {
  widgets: Widgets[];
  widgetId: number;

  constructor(
    private route: ActivatedRoute,
    private widgetService: WidgetsService,
  ) { }

  ngOnInit() {
    this.getWidgets();
    this.widgetId = null;
  }

  getWidgets(): void {
    this.widgetService.getWidgets().subscribe(widgets => this.widgets = widgets);
  }

  setWidget(id: number): void {
    this.widgetId = id;
  }

}
