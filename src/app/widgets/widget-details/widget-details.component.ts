import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { WidgetDetails, WidgetsService } from '../widgets.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-widget-details',
  templateUrl: './widget-details.component.html',
  styles: [],
})
export class WidgetDetailsComponent implements OnChanges {
  @Input() widgetId: number;
  details: WidgetDetails;

  constructor(
    private route: ActivatedRoute,
    private widgetService: WidgetsService,
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.hasOwnProperty('widgetId') && changes.widgetId.currentValue) {
      this.getDetails();
    }
  }

  getDetails(): void {
    if (this.widgetId) {
      this.widgetService.getDetails(this.widgetId).subscribe(details => this.details = details);
    }
  }

}
