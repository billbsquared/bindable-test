import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetDetailsComponent } from './widget-details/widget-details.component';
import { WidgetsComponent } from './widgets.component';
import { AppRoutingModule } from '../app-routing.module';
import { MaterialModule } from '../modules/material.module';
import { WidgetsService } from './widgets.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    WidgetDetailsComponent,
    WidgetsComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
  ],
  exports: [
    WidgetsComponent,
    WidgetDetailsComponent,
  ],
  providers: [
    WidgetsService,
  ],
})
export class WidgetsModule {}
